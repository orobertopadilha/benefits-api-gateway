import { authHelper } from 'auth-lib'

const HEADER_AUTHORIZATION = 'Authorization'
const HEADER_USER_AUTH_ID = 'user-auth-id'

export const authInterceptor = (req, res, next) => { 
    if (req.url !== "/auth/login" && req.url !== "/user") {
        let token = req.header(HEADER_AUTHORIZATION)
        let payload = authHelper.readToken(token)

        if (payload !== null) {
            req.headers[HEADER_USER_AUTH_ID] = payload.id
        } else {
            res.status(401)
            res.end()    
            return
        }
    }

    next()
}
