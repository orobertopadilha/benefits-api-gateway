import httpProxy from 'express-http-proxy'

const userProxy = httpProxy('http://localhost:8081')

const handleUserProxy = (req, res, next) => {
    if (req.url.startsWith('/user') || (req.url.startsWith('/auth'))) {
        userProxy(req, res, next)
    } else {
        next()
    }
}

export default handleUserProxy